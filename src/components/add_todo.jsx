import React, { Component } from "react";

class Add extends Component {
  render() {
    return (
      <ul>
        {this.props.data.map((item) => {
          return (
            <div
              className={
                item.isCompleted ? "d-flex holder checked " : "d-flex holder "
              }
              id={item.value}
            >
              <input
                onClick={(e) => {
                  this.props.handleCheckbox(e, item);
                }}
                type="checkbox"
                className="checkbox"
                checked={item.isCompleted}
              />
              <li
                className={item.isCompleted ? "checked" : ""}
                key={item.value}
              >
                {item.value}{" "}
              </li>
            </div>
          );
        })}
      </ul>
    );
  }
}

export default Add;

//className={item.isCompleted ? "d-flex checked": 'd-flex '}
