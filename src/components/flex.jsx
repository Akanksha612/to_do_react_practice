import React, { Component } from "react";
import Bottom from "./bottom";
import Add from "./add_todo";
import Input from "./input";
import "../App.css";

class Flex extends Component {
  constructor(props) {
    super(props);

    this.state = { data: [] };

    this.count = 0;
    this.main_array = [];
  }

  addItem = (event) => {
    if (event.charCode === 13) {
      if (event.target.value === "") {
        alert("No task given");
      } else {
        this.main_array.push({ value: event.target.value, isCompleted: false });
        this.setState({ data: this.main_array });

        event.target.value = "";

        this.count++;
        this.update_counter();
      }
    }
  };

  handleAll = (e) => {
    this.setState({ data: this.main_array });
  };

  handleActive = (e) => {
    let active_array = this.state.data.filter((todo) => {
      return todo.isCompleted === false;
    });

    this.setState({ data: active_array });
  };

  handleCompleted = (e) => {
    let completed_array = this.main_array.filter((todo) => {
      return todo.isCompleted === true;
    });

    this.setState({ data: completed_array });
  };

  handleClearCompleted = (e) => {
    let updatedArray = [];

    this.main_array.forEach((todo) => {
      if (todo.isCompleted === false) {
        updatedArray.push(todo);

        let main_arr = this.main_array.filter((todo) => {
          return todo.isCompleted === false;
        });

        this.main_array = main_arr;
      }
    });

    this.setState({
      data: updatedArray,
    });
  };

  render() {
    return (
      <div className="main-flex">
        <div className="center-flex">
          <Input addItem={this.addItem} />

          <Add data={this.state.data} handleCheckbox={this.handleCheckbox} />

          <Bottom
            all={this.handleAll}
            active={this.handleActive}
            completed={this.handleCompleted}
            clearCompleted={this.handleClearCompleted}
          />
        </div>
      </div>
    );
  } //render

  handleCheckbox = (e, item) => {
    if (e.target.checked) {
      e.target.parentElement.classList.add("checked");
      item.isCompleted = item.isCompleted ? false : true;
      this.setState({ data: this.state.data });

      this.count--;
      this.update_counter();
    } else {
      e.target.parentElement.classList.remove("checked");
      item.isCompleted = item.isCompleted ? false : true;
      this.setState({ data: this.state.data });

      this.count++;
      this.update_counter();
    }
  };

  update_counter() {
    let counter = document.getElementById("counter");

    if (this.count === 0) {
      counter.textContent = "No items left";
    } else {
      counter.textContent = this.count + " items left";
    }
  }
}

export default Flex;
