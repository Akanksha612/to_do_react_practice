import React, { Component } from "react";
class Bottom extends Component {
  render() {
    return (
      <div className="d-inline-flex align-items-center">
        <span id="counter"> No items left</span>
        <button className="m-2" onClick={this.props.all}> All</button>
        <button className="m-2" onClick={this.props.active}> Active</button>
        <button className="m-2" onClick={this.props.completed}> Completed</button>
        <button className="m-2" onClick={this.props.clearCompleted}> Clear Completed</button>
      </div>
    );
  }
}

export default Bottom;
