import React, { Component } from "react";
import "../App.css";

class Input extends Component {
  render() {
    return (
      <div clasName="d-inline-flex input_class">
        <input type="checkbox" />
        <input
          className="input_box"
          onKeyPress={this.props.addItem}
          type="text"
          placeholder="Create a new todo..."
        />
      </div>
    );
  }
}

export default Input;
